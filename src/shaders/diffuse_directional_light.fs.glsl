#version 330

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

uniform vec3 uLightDirection;
uniform vec3 uLightIntensity;

out vec3 fColor;

vec3 f(vec3 wi, vec3 w0){
    return vec3(1/3.14);
}

void main()
{  
    vec3 viewSpaceNormal=normalize(vViewSpaceNormal);
    fColor = f(uLightDirection,vViewSpacePosition)*uLightIntensity*dot(viewSpaceNormal, uLightDirection);
}